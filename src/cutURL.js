import { Storage } from "./data/Storage.js";

export const cutURL = (req, res) => {
  const urlRegex = /^(https?:\/\/)?[\d\w]+\.[\w]+(\/.*)*/;

  if (req.body.url === undefined || !urlRegex.test(req.body.url)) 
    return res.status(440).send("Bad Request :/ ");

  const code = "xxxxx".replace(/x/g, () =>
    Math.floor(Math.random() * 16).toString(16)
  );

  Storage.data.links.push({
    url: req.body.url,
    code: code,
  });
  Storage.write();

  res.status(200).send({
    code:code,
  });
  
};



