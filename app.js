import express from "express";

import { Storage } from "./src/data/Storage.js";

import { redirect } from "./src/redirect.js";

import { cutURL } from "./src/cutURL.js";

const app = express();
const port = 8080;

app.use(express.json());

if (Storage.data.links === undefined) {
  Storage.data.links = [];
  Storage.write();
}

app.post("/test", cutURL);
app.get("/[a-f0-9]{5}", redirect)

app.listen(port, () => {
  console.log(`Server now running on port ${port}!`);
})
